//
//  LikeRatioGraph.h
//  Wyldfire
//
//  Created by Danny Anderson on 2/19/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeRatioGraph : UIView

- (void)setNumLikes:(int)likes andTimesBeenLiked:(int)liked;
- (void)animate;

@end
