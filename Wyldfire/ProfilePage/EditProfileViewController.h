//
//  EditProfileViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/22/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Constants.h"
#import "InfoTableViewCell.h"
#import "ImageTableViewCell.h"

#import "UITextField+done.h"

@interface EditProfileViewController : ViewController

@end
