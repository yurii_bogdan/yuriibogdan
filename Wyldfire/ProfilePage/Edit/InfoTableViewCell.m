//
//  InfoTableViewCell.m
//  Wyldfire
//
//  Created by Danny Anderson on 3/22/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "InfoTableViewCell.h"

@interface InfoTableViewCell () <UITextFieldDelegate>

@end

@implementation InfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addImageView];
        [self addTextField];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];
}

- (CGFloat)itemHeight
{
    return EDIT_PROFILE_INFOCELL_HEIGHT;
}

- (CGFloat)pad
{
    return 6.0f;
}

- (void)addImageView
{
    CGFloat pad = [self pad];
    CGFloat sideLength = self.itemHeight - pad * 2;
    
    CGRect rect = CGRectMake(0, pad, sideLength, sideLength);
    
    UIImageView* imgView = [[UIImageView alloc] initWithFrame:rect];
    imgView.center = CGPointMake(EDIT_PROFILE_TEXT_INSET / 2, imgView.centerY);
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imgView];
    self.infoImageView = imgView;
}

- (void)addTextField
{
    CGFloat pad = [self pad];
    CGRect rect = CGRectMake(EDIT_PROFILE_TEXT_INSET,
                             0,
                             self.width - self.infoImageView.right - pad * 2,
                             self.itemHeight);
    
    UITextField* textField = [[UITextField alloc] initWithFrame:rect];
    textField.delegate = self;
    textField.textAlignment = NSTextAlignmentLeft;
    textField.font = FONT_BOLD(15);
    textField.returnKeyType = UIReturnKeyDone;
    
    [self addSubview:textField];
    self.textField = textField;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    BOOL isPhone = [self.defaultsKey isEqualToString:@"phoneNumber"];
    
    NSString* value = textField.text;
    if (isPhone) {
        value = [value filteredDigitsOfPhoneNumber];
        ECPhoneNumberFormatter *formatter = [[ECPhoneNumberFormatter alloc] init];
        value = [formatter stringForObjectValue:value];
        textField.text = value;
    }
    
    [[GVUserDefaults standardUserDefaults] setValue:value forKey:self.defaultsKey];
    
    NSString* accountField = ([self.defaultsKey rangeOfString:@"email"].location == NSNotFound ? @"phone" : @"email");
    
    [[APIClient sharedClient] updateAccountField:accountField value:value success:nil failure:^{
        
    }];
}

@end
