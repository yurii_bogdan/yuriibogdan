//
//  ImageTableViewCell.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/22/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UILabel+util.h"

@interface ImageTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic) BOOL isAvatar;

@end
