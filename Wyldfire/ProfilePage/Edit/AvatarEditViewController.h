//
//  AvatarEditViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/22/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "WFZoomView.h"

@interface AvatarEditViewController : ViewController

@end
