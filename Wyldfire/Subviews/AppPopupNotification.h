//
//  AppPopupNotification.h
//  Wyldfire
//
//  Created by Danny Anderson on 5/8/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopupNotification.h"

@interface AppPopupNotification : PopupNotification

+ (instancetype)showPopUpWithTitle:(NSString*)title
                  subtitle:(NSString*)subtitle
                    action:(NSString*)action
                   account:(Account*)account
    inNavigationController:(UINavigationController*)nav;

@end
