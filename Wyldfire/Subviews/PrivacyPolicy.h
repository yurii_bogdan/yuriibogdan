//
//  PrivacyPolicy.h
//  Wyldfire
//
//  Created by Danny Anderson on 5/5/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"
#import "Constants.h"

@interface PrivacyPolicy : UIWebView

@end
