//
//  WFZoomView.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/31/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WFZoomView : UIView

@property (nonatomic, retain)   UIImageView     *imageView;

- (id)initWithImage:(UIImage*)image andFrame:(CGRect)frame;

@end
