//
//  InfoUnderlay.h
//  Wyldfire
//
//  Created by Danny Anderson on 2/23/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Account.h"

@interface InfoUnderlay : UIView

@property (nonatomic, strong) Account* account;

@end
