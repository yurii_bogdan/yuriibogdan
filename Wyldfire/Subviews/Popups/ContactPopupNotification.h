//
//  ContactPopupNotification.h
//  Wyldfire
//
//  Created by Danny Anderson on 5/8/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "AppPopupNotification.h"

@interface ContactPopupNotification : AppPopupNotification

+ (instancetype)showContactPopup:(Account*)account
        inNavigationController:(UINavigationController*)nav;

@end
