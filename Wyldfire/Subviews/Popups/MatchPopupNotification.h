//
//  MatchPopupNotification.h
//  Wyldfire
//
//  Created by Danny Anderson on 5/8/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "AppPopupNotification.h"

@interface MatchPopupNotification : AppPopupNotification

+ (instancetype)showMatchPopup:(Account*)account
    inNavigationController:(UINavigationController*)nav;

@end
