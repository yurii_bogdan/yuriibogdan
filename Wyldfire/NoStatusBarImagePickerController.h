//
//  NoStatusBarImagePickerController.h
//  Wyldfire
//
//  Created by Danny Anderson on 5/31/14.
//  Copyright (c) 2014 Wyldfire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoStatusBarImagePickerController : UIImagePickerController

@end
