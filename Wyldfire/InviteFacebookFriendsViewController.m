//
//  InviteFacebookFriends.m
//  Wyldfire
//
//  Created by Vlad Seryakov on 11/23/13.
//  Copyright (c) 2013 Wyldfire, Inc. All rights reserved.
//
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>

@interface InviteFacebookFriendsViewController () <MFMessageComposeViewControllerDelegate>
@property (nonatomic) CGRect nextRect;
@end

@implementation InviteFacebookFriendsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addTable];
    self.tableRows = 2;
    self.table.allowsMultipleSelection = YES;
    self.table.backgroundColor = GRAY_8;
    self.tableSearch.placeholder = @"First Last";
    self.tableSearch.frame = CGRectInset(self.tableSearch.frame, 5, 0);
    
    [self addToolbar:@""];
    [self loadFriends];
    [self updateToolbarTitle];
}

- (void)updateToolbarTitle
{
    int invitesRemaining = 3 - [WFCore get].accountStructure.inviteCount;
    self.toolbarTitle.text = [NSString stringWithFormat:@"%i Invite%@ Left", invitesRemaining, invitesRemaining > 1 ? @"s" : @""];
}

- (void)loadFriends
{
    CFErrorRef error = nil;
    ABAddressBookRef book = ABAddressBookCreateWithOptions(NULL, &error);
    if (!book) return;
    ABAddressBookRequestAccessWithCompletion(book, ^(bool granted, CFErrorRef error) {
        if (!granted) return;
        NSMutableArray* phoneContacts = [NSMutableArray new];
        
        NSArray *contacts = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(book));
        for (int i = 0; i < contacts.count; i++) {
            ABRecordRef person = (__bridge ABRecordRef) [contacts objectAtIndex:i];

            ABMultiValueRef phones = ABRecordCopyValue(person, kABPersonPhoneProperty);
            if (!phones) continue;
            
            NSMutableDictionary *item = [@{} mutableCopy];
            item[@"phones"] = [@[] mutableCopy];
            for (CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
                [item[@"phones"] addObject:CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, i))];
            }
            
            // Apparently if exported from Exchange servers these can actually be NULL hence the check
            // Not sure if you want to continue or what else you wish to do here, but this will avoid the
            // Crashing that occurred in #14
            NSString *compositeName = CFBridgingRelease(ABRecordCopyCompositeName(person));
            if (compositeName == nil) {
                CFRelease(phones); // Calling to avoid leaking the object
                continue;
            }

            item[@"name"] = compositeName;
            if (ABPersonHasImageData(person)) {
                item[@"icon"] = [UIImage imageWithData:(NSData *)CFBridgingRelease(ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail))];
            }
            
            
            [phoneContacts addObject:item];
            CFRelease(phones); // Calling to avoid leaking the object
        }
        [phoneContacts sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
        self.itemsAll = phoneContacts;
        
        CFRelease(book);
        [self performSelectorOnMainThread:@selector(reloadTable) withObject:self waitUntilDone:YES];
    });
}

- (void) onCheckAll:(id)sender
{
    NSArray *selected = [self.table indexPathsForSelectedRows];
    for (int i = 0; i < self.items.count; i++) {
        const NSUInteger idx[2] = { 0, i + 3 };
        NSIndexPath *path = [NSIndexPath indexPathWithIndexes:idx length:2];
        [self onTableSelect:path selected:selected.count ? NO : YES];
        if (selected.count == 0) {
            [self.table selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
        } else {
            [self.table deselectRowAtIndexPath:path animated:NO];
        }
    }
}

- (BOOL)isMale
{
    return [WFCore get].accountStructure.isMale;
}


- (void)onTableSelect:(NSIndexPath *)indexPath selected:(BOOL)selected
{
    [self.view endEditing:YES];
    if (indexPath.row < 2) return;
    
    if (selected) {
        NSDictionary* user = self.items[indexPath.row - 2];
        [self showSMStoRecipients:@[user[@"phones"][0]]];
    }
}

- (void)onTableCell:(UITableViewCell *)cell indexPath:(NSIndexPath *)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case 0: {
            cell.backgroundColor = nil;
            CGFloat xStart = 16;
            CGRect firstLabelRect = CGRectMake(xStart,
                                               14,
                                               cell.frame.size.width - 7,
                                               17);
            UILabel *label = [[UILabel alloc] initWithFrame:firstLabelRect];
            label.textAlignment = NSTextAlignmentLeft;
            label.textColor = GRAY_2;
            label.font = [UIFont fontWithName:BOLD_FONT size:14];
            label.text = @"Share Wyldfire";
            [cell addSubview:label];
            
            CGRect subLabelRect = CGRectMake(xStart,
                                               CGRectGetHeight(firstLabelRect) + 14,
                                               cell.frame.size.width - 7,
                                               15);
            UILabel *sublabel = [[UILabel alloc] initWithFrame:subLabelRect];
            sublabel.textAlignment = NSTextAlignmentLeft;
            sublabel.textColor = GRAY_2;
            sublabel.font = [UIFont fontWithName:BOLD_FONT size:12];
            sublabel.text = @"Our network is only as good as your taste";
            [cell addSubview:sublabel];
            break;
        }
        case 1: {
            self.tableSearch.frame = CGRectInset(cell.frame, 10, 6.75);
            self.tableSearch.textAlignment = NSTextAlignmentLeft;
            cell.backgroundColor = [UIColor whiteColor];
            [cell addSubview:self.tableSearch];
            break;
        }
        default: {
            CGFloat rowHeight = [self tableView:self.table heightForRowAtIndexPath:indexPath];
            
            NSDictionary *item = [self getItem:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]];
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_circle"]];
            
            UIImageView *avatar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"avatar_eclipse"]];
            avatar.center = CGPointMake(avatar.image.size.width/2 + self.tableIndent, rowHeight/2 );
            [cell addSubview:avatar];
            
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, avatar.image.size.width-3, avatar.image.size.width-3)];
            imgView.center = avatar.center;
            imgView.contentMode = UIViewContentModeScaleAspectFill;
            imgView.layer.cornerRadius = imgView.frame.size.width/2;
            imgView.layer.masksToBounds = YES;
            [cell addSubview:imgView];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(avatar.frame.size.width+self.tableIndent*2, 0, cell.frame.size.width/2, rowHeight)];
            label.text = item[@"name"];
            label.textColor = [UIColor darkGrayColor];
            label.tag = 900;
            [cell addSubview:label];
            
            if (item[@"icon"]) {
                imgView.image = item[@"icon"];
            } else {
                imgView.alpha = 0.0;
                avatar.alpha = 0.0;
                cell.accessoryView.alpha = 0.0;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            return 50;
        case 1:
            return 43;
        default:
            return 44;
    }
}

- (void)showSMStoRecipients:(NSArray*)recipients
{
    if(![MFMessageComposeViewController canSendText]) {
//        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [warningAlert show];
        
        [WFCore showAlert:@"Error" msg:@"Your device doesn't support SMS!" delegate:nil confirmHandler:nil];
        return;
    }
    
    NSString *message = @"Hey, I think you would make a great addition to Wyldfire’s network. Download the app here: AppStore.com/Wyldfire or watch the commercial at youtu.be/BagXyAops9E";

    if ([WFCore get].accountStructure.isMale) message = @"Hey, do you think I belong on Wyldfire? It’s invite only and I need your approval :) Download the app here: AppStore.com/Wyldfire or watch the commercial at youtu.be/BagXyAops9E";
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipients];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
//            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [warningAlert show];
            
            [WFCore showAlert:@"Error" msg:@"Failed to send SMS" delegate:nil confirmHandler:nil];
            break;
        }
            
        case MessageComposeResultSent:{
            //NSArray* recipients = controller.recipients;
            //            if (![self isMale]) {
//                for (NSString* recipient in recipients) {
//                    [[APIClient sharedClient] sendInviteToPhoneNumber:recipient
//                                                              success:^{
//                                                                  [WFCore get].accountStructure.hasFeather = YES;
//                                                                  [WFCore get].accountStructure.inviteCount++;
//                                                                  if ([WFCore get].accountStructure.inviteCount == 3) {
//                                                                      [self.navigationController popViewControllerAnimated:YES];
//                                                                      controller.delegate = nil;
//                                                                  } else {
//                                                                      [self updateToolbarTitle];
//                                                                  }
//                                                              } failure:^{
//                                                                  [WFCore showAlert:@"We are unable to send this invite" text:@"Please make sure the number is correct or try again later." delegate:nil cancelButtonText:@"OK" otherButtonTitles:nil tag:0];
//                                                              }];
//                }
//            }
//            break;
        }
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
