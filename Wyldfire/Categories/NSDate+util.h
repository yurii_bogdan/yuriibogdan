//
//  NSDate+util.h
//  Wyldfire
//
//  Created by Danny Anderson on 4/9/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (util)

- (NSDate *) toLocalTime;
- (NSDateComponents*)components;

@end
