//
//  UIActionSheet+util.h
//  Wyldfire
//
//  Created by Danny Anderson on 2/21/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActionSheet (util)

- (void) styleWithTintColor:(UIColor*)tintColor;

@end
