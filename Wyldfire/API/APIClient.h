//
//  APIClient.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/12/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Crashlytics/Crashlytics.h>

#import "AFNetworking.h"
#import "CoreData.h"

#import "NSString+util.h"
#import "GVUserDefaults+WF.h"

#import "FacebookUtility.h"

#import "Account.h"
#import "Stats.h"

#import "DBAccount+util.h"
#import "Message+util.h"

#import "UnderReViewController.h"
#import "WebViewViewController.h"

@interface APIClient : AFHTTPClient

@property (strong, nonatomic) FBSession *session;
@property (strong, nonatomic) NSString *browseToken;

+ (instancetype)sharedClient;

// Complicated Sign Up Flow

- (void)checkGeofenceWithSuccess:(GenericBlock)success
                      andFailure:(FailureBlock)failure;
- (void)checkInviteCode:(NSString*)code success:(GenericBlock)success failure:(void (^)(NSString* reason))failure;
- (void)getInviteCode:(SuccessBlock)success failure:(GenericBlock)failure;


//Management
- (void)checkFacebookStatus:(GenericBlock)success;
- (void)clearQueue;
- (BOOL)connectedViaWifi;
- (BOOL)online;

- (void)facebookLogin:(UIViewController*)viewController successBlock:(GenericBlock)success failureBlock:(GenericBlock)failure;
- (void)getAccount:(void (^)(Account *account))success failure:(GenericBlock)failure;
- (void)deleteAccount:(GenericBlock)success failure:(GenericBlock)failure;
- (void)getUserAccount:(NSString*)wyldfireID
       withObjectOrNil:(Account*)account
               success:(void (^)(Account *account, NSDictionary* json))success
               failure:(GenericBlock)failure;
- (void)updateAccountField:(NSString*)field value:(id)value
                   success:(GenericBlock)success failure:(GenericBlock)failure;

//Images
- (void)uploadImage:(UIImage*)image type:(NSInteger)type success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)setImageFromURL:(NSString*)url type:(NSInteger)type success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)downloadImage:(NSString*)url success:(ImageSuccessBlock)success failure:(FailureBlock)failure;
- (void)getAccountImageOfType:(int)type accountIDOrNil:(NSString*)accountID
                      success:(ImageSuccessBlock)success failure:(FailureBlock)failure;

//Sign Up Flow
- (void)nextActionAfterLogin:(UIViewController*)viewController;
- (void)addAccount:(GenericBlock)success failure:(FailureBlock)failure;

//Location
- (void)putLocation:(CLLocation*)location success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)getNearbyAccounts:(void (^)(NSArray* accounts))success failure:(GenericBlock)failure;
- (void)getTrendingForGenderString:(NSString*)genderString success:(void (^)(NSArray* accounts))success failure:(GenericBlock)failure;

//To be used by Social Account
- (void)getJSON:(NSString *)url method:(NSString*)method params:(NSDictionary*)params headers:(NSDictionary*)headers success:(SuccessBlock)success failure:(JSONFailureBlock)failure;

//Connections
- (void)likeUser:(Account*)userAccount
         success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)burnUser:(Account*)userAccount
         success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)passUser:(Account*)userAccount
         success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)hintUser:(Account*)userAccount
         success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)matchUser:(Account*)userAccount
         success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)viewUser:(Account*)userAccount
         success:(GenericBlock)success failure:(GenericBlock)failure;


- (void)checkIfLikedbyUser:(Account*)userAccount
                   success:(void (^)(BOOL connectionExists))success failure:(GenericBlock)failure;

- (void)checkIfHintedbyUser:(Account*)userAccount
                    success:(void (^)(BOOL connectionExists))success failure:(GenericBlock)failure;

//Blackbook
- (void)shareContactInfoWithAccount:(Account*)account
                            success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)getBlackbookWithSuccess:(void (^)(NSArray* accounts))success failure:(GenericBlock)failure;

//Matches
- (void)storePendingMatches;    //Convenient if don't need progress notification
- (void)getPendingMatches:(void (^)(NSArray* matches))success failure:(GenericBlock)failure;
- (void)storePendingMatches:(NSArray*)matches;
//Retrieval methods in DBAccount+util.h

//Messages
- (void)putMessage:(NSString*)message toID:(NSString*)toID
           success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)putMessageImage:(UIImage*)image text:(NSString*)text toID:(NSString*)toID
                success:(GenericBlock)success failure:(GenericBlock)failure;
- (void)downloadAllMessages;
//Retrieval methods in Message+util.h


//Hints
- (void)checkHintsUsedInLast24Hours:(void (^)(int hintsUsed))block;

//Report User
- (void)checkIfReportedWithSuccess:(void (^)(BOOL reported))block failure:(GenericBlock)failure;
- (void)reportUser:(Account*)userAccount
           success:(GenericBlock)success failure:(GenericBlock)failure;

//Cleanup
- (void)checkLocalAccountStatusesWithServer;

@end
