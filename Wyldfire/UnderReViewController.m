//
//  UnderReViewController.m
//  Wyldfire
//
//  Created by Danny Anderson on 5/5/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "UnderReViewController.h"

@interface UnderReViewController ()

@end

@implementation UnderReViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupImageView];
    [self subscribeToNotifications];
}

- (void)setupImageView
{
    NSString* imgName = TALL_SCREEN ? @"underReview" : @"underReviewi4.jpg";
    UIImageView* imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
    imgView.frame = self.view.bounds;
    [self.view addSubview:imgView];
}

#pragma mark - Notifications

- (void)subscribeToNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enteredForeground)
                                                 name:NOTIFICATION_ENTERED_FOREGROUND
                                               object:nil];
}

- (void)enteredForeground
{
    [[APIClient sharedClient]  checkIfReportedWithSuccess:^(BOOL reported) {
        if (!reported) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
