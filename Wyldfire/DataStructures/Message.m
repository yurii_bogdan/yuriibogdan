//
//  Message.m
//  Wyldfire
//
//  Created by Danny Anderson on 4/9/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic created;
@dynamic image;
@dynamic mtime;
@dynamic senderAccountID;
@dynamic sent;
@dynamic text;
@dynamic updated;
@dynamic unread;

@end
