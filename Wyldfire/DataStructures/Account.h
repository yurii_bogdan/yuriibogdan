//
//  Account.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/14/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <KiipSDK/KiipSDK.h>

#import "WFCore.h"
#import "Stats.h"

@interface Account : NSObject


@property (nonatomic, strong) NSString* accountID;
@property (nonatomic) long long facebookID;

@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* facebookEmail;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* alias;
@property (nonatomic, strong) NSString* instagramID;
@property (nonatomic, strong) NSString* instagramUsername;
@property (nonatomic, strong) NSString* birthday;
@property (nonatomic, strong) NSString* fbIconString; //Not in DB
@property (nonatomic) BOOL isMale;
@property (nonatomic) int age;

@property (nonatomic) int inviteCount; //Not in DB, just for women
@property (nonatomic) BOOL hasFeather;  //Not in DB
//For other people's accounts:
@property (nonatomic) BOOL isTrending;  //Not in DB
@property (nonatomic) int distance;  //Not in DB
@property (nonatomic, strong) NSString* phone;

//Images
@property (nonatomic, strong) UIImage* avatarPhoto;
@property (nonatomic, strong) UIImage* showcasePhoto;
@property (nonatomic, strong) NSMutableDictionary* profilePhotos;

//setImage does not call the API, just changes locally
- (void)setImage:(UIImage*)image forType:(NSInteger)type;
- (UIImage*)profileImageForType:(NSInteger)type;
- (NSArray*)allProfileImages;

//References to other items
@property (nonatomic, strong) Stats* stats;

//Factory Methods
+ (instancetype)accountFromAPICall:(id)user;
+ (instancetype)accountFromAPICall:(id)user usingObject:(Account*)account;
+ (instancetype)accountFromFBUser:(id)user;

//JSON Support
- (NSDictionary*)dictionaryRepresentation;

//For ChatViewController
@property (nonatomic) int messageCount;

@end
