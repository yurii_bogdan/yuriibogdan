//
//  DBAccount.m
//  Wyldfire
//
//  Created by Danny Anderson on 5/10/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "DBAccount.h"


@implementation DBAccount

@dynamic accountID;
@dynamic age;
@dynamic alias;
@dynamic avatarPhoto;
@dynamic birthday;
@dynamic created;
@dynamic email;
@dynamic facebookEmail;
@dynamic facebookID;
@dynamic inBlackbook;
@dynamic inChat;
@dynamic isMale;
@dynamic name;
@dynamic phone;
@dynamic profilePhoto1;
@dynamic profilePhoto2;
@dynamic profilePhoto3;
@dynamic profilePhoto4;
@dynamic showcasePhoto;
@dynamic showInMatches;
@dynamic updated;
@dynamic burned;
@dynamic sentShareTo;

@end
