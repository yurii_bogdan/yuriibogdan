//
//  Account.m
//  Wyldfire
//
//  Created by Danny Anderson on 3/14/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "Account.h"

@implementation Account

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.profilePhotos = [NSMutableDictionary new];
    }
    return self;
}

- (NSDictionary*)dictionaryRepresentation
{
    return @{@"facebook_id"     :   @(self.facebookID),
             @"facebook_email"  :   self.email ?: @"",
             @"name"            :   self.name ?: @"",
             @"alias"           :   self.alias ?: @"",
             @"birthday"        :   self.birthday ?: @"",
             @"gender"          :   [self genderString],
             @"email"           :   self.email ?: @""};
}

- (NSString*)genderString
{
    return self.isMale ? @"m" : @"f";
}

+ (instancetype)accountFromAPICall:(id)user
{
    Account* account = [self new];
    return [self accountFromAPICall:user usingObject:account];
}

+ (instancetype)accountFromAPICall:(id)user usingObject:(Account*)account
{
    @try {
        account.age         = [user[@"age"] intValue];
        account.alias       = user[@"alias"];
        account.email       = user[@"facebook_email"];  //Also facebook_email, but they are the same for now
        account.facebookEmail= user[@"facebook_email"];
        account.facebookID  = [user[@"facebook_id"] longLongValue];
        account.isMale      = [self isMaleString:user[@"gender"]];
        account.accountID   = user[@"id"];
        account.name        = user[@"name"];
        account.distance    = [user[@"distance"] isKindOfClass:[NSNull class]] ? 0 : [user[@"distance"] intValue] * 0.621371;
        account.phone       = [user[@"phone"] stringValue];
        account.instagramID      = user[@"instagram_id"];
        account.instagramUsername      = user[@"instagram_username"];
        
        account.hasFeather = YES;
    }
    @catch (NSException *exception) {
        //
    }
    @finally {
        //
    }
    
    return account;
}

+ (instancetype)accountFromFBUser:(id)user
{
    Account* account = [WFCore get].accountStructure;
    
    @try {
        if (!account) {
            account = [self new];
        }
        
        account.facebookID      = [user[@"id"] longLongValue];
        account.facebookEmail   = user[@"email"];
        account.email           = user[@"email"];
        account.name            = user[@"name"];
        account.alias           = user[@"alias"];
        account.birthday        = user[@"birthday"];
        account.fbIconString    = user[@"icon"];
        
        if ([user[@"age"] isKindOfClass:[NSNumber class]]) {
            account.age = [(NSNumber*)user[@"age"] intValue];
        }
        
        //GENDER
        account.isMale          = [self isMaleString:user[@"gender"]];
        account.hasFeather      = NO;
        
        
        if([GVUserDefaults standardUserDefaults].displayEmail.length == 0) {
            [GVUserDefaults standardUserDefaults].displayEmail = account.email;
        }

    }
    @catch (NSException *exception) {
        //
    }
    @finally {
        //
    }
    
    return account;
}

- (NSString*)keyForType:(NSInteger)type
{
    return [NSString stringWithFormat:@"%i", (int)type];
}

- (void)setImage:(UIImage*)image forType:(NSInteger)type
{
    if(type == 1) {
        self.avatarPhoto = image;
    } else if (type == 0) {
        self.showcasePhoto = image;
    } else {
        [self.profilePhotos setValue:image forKey:[self keyForType:type]];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATED_ACCOUNT_PHOTOS object:self userInfo:@{@"account" : self}];
}

- (UIImage*)profileImageForType:(NSInteger)type
{
    return self.profilePhotos[[self keyForType:type]];
}

- (NSArray*)allProfileImages
{
    NSArray * sortedKeys = [[self.profilePhotos allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
    
    return [self.profilePhotos objectsForKeys:sortedKeys notFoundMarker:[NSNull null]];
}

#pragma mark - Helper Functions

+ (BOOL)isMaleString:(NSString*)genderString
{
    return [genderString isEqualToString:@"m"];
}


@end
