//
//  main.m
//  Wyldfire
//
//  Created by Vlad Seryakov on 9/9/13.
//  Copyright (c) 2013 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
