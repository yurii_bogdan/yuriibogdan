//
//  KiipTourViewController.m
//  Wyldfire
//
//  Created by Danny Anderson on 5/9/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "KiipTourViewController.h"

@interface KiipTourViewController () <UIScrollViewDelegate>
    @property (strong, nonatomic) UIScrollView* imageScrollView;
    @property (strong, nonatomic) NSMutableArray* imageViews;
    @property (strong, nonatomic) UIButton* goButton;
@end

@implementation KiipTourViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.view.bounds = [UIScreen mainScreen].bounds;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupScrollView];
    
    [self addImages];
    [self setupButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.imageScrollView setContentOffset:CGPointZero];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.imageScrollView setContentOffset:CGPointZero animated:YES];
}

- (void)setupScrollView
{
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.backgroundColor = GRAY_1;
    
    self.imageScrollView = scrollView;
    [self.view addSubview:scrollView];
    
    self.imageViews = [NSMutableArray new];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
}

- (UIImageView*)imageView
{
    UIImageView* imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imgView.contentMode = UIViewContentModeScaleToFill;
    imgView.clipsToBounds = YES;
    
    return imgView;
}

- (void)addImages
{
    for (int i = 1; i <=2; i++) {
        NSString* imgName = [NSString stringWithFormat:@"Kiip_Tour_%i%@.jpg", i, TALL_SCREEN ? @"" : @"i4"];
        UIImage* image = [UIImage imageNamed:imgName];
        UIImageView* imgView = [self imageView];
        
        [self.imageViews addObject:imgView];
        imgView.image = image;
    }
    
    [self updateScrollview];
}

- (void)updateScrollview
{
    NSInteger numberOfImages = self.imageViews.count;
    
    [self.imageScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat width = CGRectGetWidth(self.view.frame);
    CGFloat height = CGRectGetHeight([UIScreen mainScreen].bounds);
    [self.imageScrollView setContentSize:CGSizeMake(width * numberOfImages, height)];
    
    for (int i = 0; i < numberOfImages; i++) {
        UIImageView* imgView = self.imageViews[i];
        
        CGRect frame = CGRectMake(i * width,
                                  0,
                                  width,
                                  height);
        imgView.frame = frame;
        [self.imageScrollView addSubview:imgView];
    }
    [self.imageScrollView scrollRectToVisible:self.view.bounds animated:NO];
}

- (void)setupButton
{
    UIButton* button =[[UIButton alloc] initWithFrame:CGRectMake(0,
                                              self.view.height - 80,
                                              self.view.width,
                                              80)];
    [button addTarget:self action:@selector(pressedGo) forControlEvents:UIControlEventTouchUpInside];
    self.goButton = button;
    [self.view addSubview:button];
}

- (void)pressedGo
{
    [self.navigationController popViewControllerAnimated:YES];
    [self.poptart show];
}

@end
