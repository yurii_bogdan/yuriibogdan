//
//  AppTourViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 2/21/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageViewController.h"
#import "APIClient.h"
#import "GVUserDefaults+WF.h"

#import <MediaPlayer/MediaPlayer.h>

#import "LocationServicesViewController.h"
#import "NoInternetViewController.h"

@interface AppTourViewController : ImageViewController <UIScrollViewDelegate>

@end
