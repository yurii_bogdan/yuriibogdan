//
//  KiipTourViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 5/9/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KiipTourViewController : UIViewController

@property (nonatomic, strong) KPPoptart* poptart;

@end
