//
//  WaitingRoomFriendsViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/17/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookUtility.h"

@interface WaitingRoomFriendsViewController : InviteFacebookFriendsViewController

@end
