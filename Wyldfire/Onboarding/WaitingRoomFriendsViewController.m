//
//  WaitingRoomFriendsViewController.m
//  Wyldfire
//
//  Created by Danny Anderson on 3/17/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "WaitingRoomFriendsViewController.h"

@implementation WaitingRoomFriendsViewController

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)isMale
{
    return [WFCore get].accountStructure.isMale;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.table.backgroundColor = GRAY_1;
    self.toolbarTitle.text = [self isMale] ? @"Request Feather" : @"Send Feather";
    self.toolbar.backgroundColor = GRAY_1;
    self.tableSearch.backgroundColor = GRAY_1;
    self.tableSearch.leftView.backgroundColor = GRAY_1;
    
    self.toolbarTitle.textColor = WYLD_RED;
    self.tableSearch.textColor = [UIColor whiteColor];
    
    UIColor *color = [UIColor whiteColor];
    self.tableSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First, Last" attributes:@{NSForegroundColorAttributeName: color}];
}

- (void)updateToolbarTitle
{
    //Noop
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        UIView* hideLine = [[UIView alloc] initWithFrame:cell.bounds];
        hideLine.backgroundColor = GRAY_1;
        [cell addSubview:hideLine];
        
        NSString* text = [self isMale] ? @"To get a feather from one of your female\nfriends, tap to send a request via text." :
                        @"Women screen which men can join Wyldfire.\nPlease invite 1-3 quality guys to the network.";
        
        UILabel* label = [UILabel labelInRect:CGRectInset(cell.bounds,8,0)
                                     withText:text
                                        color:[UIColor whiteColor] fontSize:15];
        label.backgroundColor = GRAY_1;
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:label];
    } else if (indexPath.row == 1) {
        cell.backgroundColor = nil;
    } else if (indexPath.row > 1) {
        [(UIImageView*)cell.accessoryView setImage:(cell.selected ? [UIImage imageNamed:@"red_check"] : nil)];
        UILabel *label = (UILabel*)[cell viewWithTag:900];
        label.textColor = [UIColor whiteColor];
    }
}

@end
