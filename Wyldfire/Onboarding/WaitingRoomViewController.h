//
//  WaitingRoomViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 2/21/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//
#import <MediaPlayer/MediaPlayer.h>

#import "ImageViewController.h"
#import "SidebarProfileView.h"
#import "ShiftedTextField.h"
#import "UILabel+util.h"
#import "Constants.h"

@interface WaitingRoomViewController : ImageViewController

@end
