//
//  NoInternetViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 5/8/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+util.h"

@interface NoInternetViewController : UIViewController

+ (void)showNoInternetViewControllerInNavController:(UINavigationController*)nav;

@end
