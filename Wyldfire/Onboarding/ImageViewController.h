//
//  ImageViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 2/21/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

- (void)addObscurants;
- (UIImageView*)imageView;
- (CGRect)bottomButtonRect;
- (UIButton*)addButtonWithTitle:(NSString*)title backgroundColor:(UIColor*)bgColor frame:(CGRect)frame selectorString:(NSString*)selectorString;

@end
