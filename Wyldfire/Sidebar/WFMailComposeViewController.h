//
//  WFMailComposeViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/18/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <MessageUI/MessageUI.h>

@interface WFMailComposeViewController : MFMailComposeViewController

@end
