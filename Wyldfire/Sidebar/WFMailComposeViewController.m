//
//  WFMailComposeViewController.m
//  Wyldfire
//
//  Created by Danny Anderson on 3/18/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "WFMailComposeViewController.h"

@interface WFMailComposeViewController ()

@end

@implementation WFMailComposeViewController

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(UIViewController *)childViewControllerForStatusBarStyle
{
    return nil;
}

@end
