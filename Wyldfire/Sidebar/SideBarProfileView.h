//
//  SideBarProfileView.h
//  Wyldfire
//
//  Created by Danny Anderson on 2/19/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BorderedLabel.h"

@interface SideBarProfileView : UIView

@property (nonatomic, strong) UIImageView* profileImage;
@property (nonatomic, strong) UILabel* profileName;
@property (nonatomic, strong) UILabel* profileLikes;

- (void)animate;

@end
