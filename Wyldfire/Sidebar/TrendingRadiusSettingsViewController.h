//
//  TrendingRadiusSettingsViewController.h
//  Wyldfire
//
//  Created by Danny Anderson on 3/13/14.
//  Copyright (c) 2014 Wyldfire, Inc. All rights reserved.
//

#import "RangeSliderSettingsViewController.h"

@interface TrendingRadiusSettingsViewController : RangeSliderSettingsViewController

@end
